package application;
	
import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		vars.primaryStage = primaryStage;
		initRootLayout();
	}
	
	public void initRootLayout() {
        try {
        	vars.show = false;
        	
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("Notice.fxml"));
            vars.root = (AnchorPane) loader.load();
           

            Dimension sSize = Toolkit.getDefaultToolkit ().getScreenSize ();
            
            
            Scene scene = new Scene(vars.root);
            vars.primaryStage.setScene(scene);
            vars.primaryStage.initStyle(StageStyle.UNDECORATED);
            vars.primaryStage.setY(0);
            vars.primaryStage.setAlwaysOnTop(true);
            vars.primaryStage.setX(sSize.width-200);
            
            vars.primaryStage.show();
            vars.show = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	public static void main(String[] args) {
		launch(args);
	}
}
