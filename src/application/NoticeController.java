package application;







import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.Timer;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

public class NoticeController {
	private Timer t;
	
	@FXML
	private Label TitleName;
	@FXML
	private Label Series;
	@FXML
	private AnchorPane AppBody;
	@FXML
	private Label version;
	@FXML
	private void initialize() {
		
		
		t = new Timer(100, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(vars.show) {
					Platform.runLater(new Runnable() {
				        @Override
				        public void run() {
				        		version.setText(vars.version);
				        		vars.primaryStage.hide();
				        		vars.RefreshFromSite();
								TitleName.setText(vars.lastTitle);
								Series.setText(vars.lastSeries);
							}
				   });
					t.stop();
				}
			}});
		t.start();
		
	}
	@FXML
	public void closeApp() {
		vars.primaryStage.hide();
		vars.RefreshFromSite();
		TitleName.setText(vars.lastTitle);
		Series.setText(vars.lastSeries);
		
	}
	@FXML
	public void toUrl() {
		if (Desktop.isDesktopSupported()) {
		    try {
				Desktop.getDesktop().browse(new URI(vars.lastUrl));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		closeApp();
		
	}
}
