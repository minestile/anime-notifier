package application;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class vars {
	public static boolean show = true;
	public static Stage primaryStage;
	public static Stage splashStage;
	public static AnchorPane root;
	public static Scene scene;
	public static Timestamp timestamp;
	public static String lastTitle = "";
	public static String dlastTitle = "";
	public static String lastSeries = "";
	public static String lastUrl = "";
	
	
	public static String version = "0.2";
	
	
	
	public static void getFromSite() {
		try {
			Document doc = Jsoup.connect("http://animevost.org/rss.xml").get();
			Element l = doc.getElementsByTag("item").get(0);
			lastTitle = l.getElementsByTag("title").get(0).text();
			lastUrl = l.getElementsByTag("link").get(0).text();
			String tmp = "";
			try {
				lastSeries = lastTitle;
				//lastSeries = Pattern.compile("\\[(.*?)\\]").matcher(lastTitle);
				tmp = lastSeries.replaceAll("\\[(.*?)\\]", "").replace(" ", "");
				lastSeries = lastSeries.replace(" ", "").replace(tmp, "");
			}catch(IllegalStateException e) {
				e.printStackTrace();
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void RefreshFromSite() {
		timestamp = new Timestamp(System.currentTimeMillis());
		while(true) {
			Timestamp tp = new Timestamp(System.currentTimeMillis());
			if(tp.getTime()-timestamp.getTime() > 60000) {
				getFromSite();

				if(vars.show && !dlastTitle.equals(lastTitle)) {
					vars.primaryStage.show();
					dlastTitle = lastTitle;
					break;
				}
				
				dlastTitle = lastTitle;
					
				
			}
				
		}
	}
}
